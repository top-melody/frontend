## Команды для управления докер-контейнером

Сборка docker-контейнера
```shell
docker build -t top-melody-frontend:latest ./docker
```

Установка зависимостей
```shell
docker run -v $(pwd):/app -it -p 8080:8080 top-melody-frontend yarn install --production=false
```

Запуск команды из докера
```shell
docker run -v $(pwd):/app -it top-melody-frontend команда
```

Запуск проекта для разработки
```shell
docker run -v $(pwd):/app -it -p 8080:8080 top-melody-frontend quasar dev
```

Сборка проекта
```shell
docker run -v $(pwd):/app -it top-melody-frontend quasar build
```
