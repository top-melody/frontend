import {defineStore} from 'pinia';
import TrackController from 'src/scripts/controllers/TrackController'

export const useTrackStore = defineStore('trackStore', {
    state: () => ({
        currentId: null,
        currentLocal: null,
        list: [],
        isPlaying: false,
        currentPosition: 0,
        isShuffle: TrackController.getInstance().getIsShuffle(),
        isRepeat: TrackController.getInstance().getIsRepeat(),
        volume: TrackController.getInstance().getVolume(),
        currentPlaylistId: null,
    }),
    getters: {
        current(state) {
            return state.list[state.currentPlaylistId].find(track => track.id === state.currentId)
        },
    },
});