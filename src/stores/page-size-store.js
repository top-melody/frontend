import {defineStore} from 'pinia';

export const usePageSizeStore = defineStore('pageSize', {
    state: () => ({
        isVerticalOrientation: true,
    }),
});