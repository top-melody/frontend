import * as ScreenOrientationEnum from 'src/scripts/enums/ScreenOrientationEnum'

export default class Utils {
    static getScreenOrientation() {
        // const verticalOrientations = ['portrait-primary', 'portrait-secondary']
        //
        // return verticalOrientations.includes(screen.orientation)
        //     ? ScreenOrientationEnum.Vertical
        //     : ScreenOrientationEnum.Horizontal
        return window.innerHeight > window.innerWidth
            ? ScreenOrientationEnum.Vertical
            : ScreenOrientationEnum.Horizontal
    }

    static isVerticalOrientation() {
        return Utils.getScreenOrientation() === ScreenOrientationEnum.Vertical
    }

    static getTimeStringFromDuration(duration) {
        const hours = Math.floor(duration / 3600);
        const minutes = Math.floor((duration - (hours * 3600)) / 60);
        const seconds = Math.floor(duration - (hours * 3600) - (minutes * 60));

        let result = ''
        if(hours > 0) result += hours.toString() + ':'

        if(minutes < 10) result += '0'
        result += minutes.toString() + ':'

        if(seconds < 10) result += '0'
        result += seconds.toString()

        return result;
    }
}
