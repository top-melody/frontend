export default class UrlHelper {
    static getParam(key) {
        return (new URL(window.location.href)).searchParams.get(key)
    }

    static setParam(key, value) {
        let url = new URL(window.location.href)
        url.searchParams.set(key, value)
        window.history.pushState({}, document.title, url.href)
    }

    static removeParam(key) {
        let url = new URL(window.location.href)
        url.searchParams.delete(key)
        window.history.pushState({}, document.title, url.href)
    }
}