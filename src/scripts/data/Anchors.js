import * as PageRouteEnum from 'src/scripts/enums/PageRouteEnum'

export default [
    {
        title: "Песня",
        link: PageRouteEnum.Track,
        icon: "play_arrow"
    },
    {
        title: "Плейлист",
        link: PageRouteEnum.Playlist,
        icon: "playlist_play"
    },
    {
        title: "Эквалайзер",
        link: PageRouteEnum.Eq,
        icon: "equalizer"
    },
    {
        title: "Аккаунт",
        link: PageRouteEnum.Account,
        icon: "person_outline"
    },
    {
        title: "О приложении",
        link: PageRouteEnum.About,
        icon: "import_contacts"
    },
]