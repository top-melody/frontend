export default [
    {
        name: 'ВКонтакте',
        url: '',
        placeholder: 'https://vk.com/durov',
        hasUpdate: false,
        lastUpdateDate: null,
        lastUpdateTime: null,
    },
    {
        name: 'Promodj',
        url: '',
        placeholder: 'https://promodj.com/jim',
        hasUpdate: true,
        lastUpdateDate: '12.01.2022',
        lastUpdateTime: '12:49',
    }
]