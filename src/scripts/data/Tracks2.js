export default [
    {
        id: 34,
        name: 'Красный май',
        authorName: 'Пахомов Олег',
        duration: 1810,
        isOnMyPlaylist: true,
    },
    {
        id: 2,
        name: 'Движ',
        authorName: 'Filatov & Karas',
        duration: 1549,
        isOnMyPlaylist: false,
    },
    {
        id: 343,
        name: 'Мы будем вместе',
        authorName: 'Artik & Asti',
        duration: 120,
        isOnMyPlaylist: true,
    },
    {
        id: 534,
        name: 'Красный май',
        authorName: 'Пахомов Олег',
        duration: 1810,
        isOnMyPlaylist: true,
    },
    {
        id: 52,
        name: 'Движ',
        authorName: 'Filatov & Karas',
        duration: 1549,
        isOnMyPlaylist: false,
    },
    {
        id: 5343,
        name: 'Мы будем вместе',
        authorName: 'Artik & Asti',
        duration: 120,
        isOnMyPlaylist: true,
    },
]