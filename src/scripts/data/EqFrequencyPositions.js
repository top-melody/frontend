import * as EqLevelKeysEnum from 'src/scripts/enums/EqLevelKeysEnum'

export default [
    {
        id: EqLevelKeysEnum.CommonLevel,
        label: 'Ур.',
        showDescription: true,
        frequency: null,
    },
    {
        id: EqLevelKeysEnum.Level40,
        label: '40 Гц',
        showDescription: false,
        frequency: 40,
    },
    {
        id: EqLevelKeysEnum.Level80,
        label: '80 Гц',
        showDescription: false,
        frequency: 80,
    },
    {
        id: EqLevelKeysEnum.Level125,
        label: '125 Гц',
        showDescription: false,
        frequency: 125,
    },
    {
        id: EqLevelKeysEnum.Level250,
        label: '250 Гц',
        showDescription: false,
        frequency: 250,
    },
    {
        id: EqLevelKeysEnum.Level400,
        label: '400 Гц',
        showDescription: false,
        frequency: 400,
    },
    {
        id: EqLevelKeysEnum.Level600,
        label: '600 Гц',
        showDescription: false,
        frequency: 600,
    },
    {
        id: EqLevelKeysEnum.Level1k,
        label: '1 кГц',
        showDescription: false,
        frequency: 1000,
    },
    {
        id: EqLevelKeysEnum.Level2k,
        label: '2 кГц',
        showDescription: false,
        frequency: 2000,
    },
    {
        id: EqLevelKeysEnum.Level4k,
        label: '4 кГц',
        showDescription: false,
        frequency: 4000,
    },
    {
        id: EqLevelKeysEnum.Level8k,
        label: '8 кГц',
        showDescription: false,
        frequency: 8000,
    },
    {
        id: EqLevelKeysEnum.Level10k,
        label: '10 кГц',
        showDescription: false,
        frequency: 10000,
    },
    {
        id: EqLevelKeysEnum.Level12k,
        label: '12 кГц',
        showDescription: false,
        frequency: 12000,
    },
    {
        id: EqLevelKeysEnum.Level16k,
        label: '16 кГц',
        showDescription: false,
        frequency: 16000,
    },
    {
        id: EqLevelKeysEnum.Level20k,
        label: '20 кГц',
        showDescription: false,
        frequency: 20000,
    }
]