import * as PlaylistEnum from 'src/scripts/enums/PlaylistEnum'

export default [
    {
        id: PlaylistEnum.MY,
        name: "Мой плейлист"
    },
    {
        id: PlaylistEnum.NEW,
        name: "Новинки"
    }
]