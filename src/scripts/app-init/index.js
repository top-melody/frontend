import { usePageSizeStore } from 'src/stores/page-size-store'
import Utils from 'src/scripts/helpers/Utils'
import tracks from 'src/scripts/data/Tracks'
import tracks2 from 'src/scripts/data/Tracks2'
import TrackController from 'src/scripts/controllers/TrackController'
import Playlists from 'src/scripts/data/Playlists'
import * as PlaylistEnum from 'src/scripts/enums/PlaylistEnum'
import AudioController from 'src/scripts/controllers/AudioController'
import ListenAudioEventsService from 'src/scripts/services/ListenAudioEventsService'
import ColorThemeController from 'src/scripts/controllers/ColorThemeController'

export default class AppInitService
{
    service() {
        this.setCurrentColorTheme()
        this.watchOrientation()
        this.initTrackList()
        this.initCurrentPlaylist()
        this.initCurrentId()
        this.initListenAudioEvents()
    }

    setCurrentColorTheme() {
        ColorThemeController.getInstance().setCurrentTheme()
    }

    watchOrientation() {
        const pageSizeStore = usePageSizeStore()
        pageSizeStore.isVerticalOrientation = Utils.isVerticalOrientation()

        window.addEventListener("resize", () => {
            const pageSizeStore = usePageSizeStore()
            pageSizeStore.isVerticalOrientation = Utils.isVerticalOrientation()
        })
    }

    initTrackList() {
        const playlistIdToTrackListMapping = {
            [PlaylistEnum.MY]: tracks,
            [PlaylistEnum.NEW]: tracks2,
        }

        for (const position in Playlists) {
            const playlistData = Playlists[position]
            TrackController.getInstance().setList(playlistData.id, playlistIdToTrackListMapping[playlistData.id])
        }
    }

    initCurrentPlaylist() {
        TrackController.getInstance().setCurrentPlaylistId(PlaylistEnum.MY)
    }

    initCurrentId() {
        const firstList = TrackController.getInstance().getList(PlaylistEnum.MY)
        TrackController.getInstance().setCurrentId(firstList[0].id)
    }

    initListenAudioEvents() {
        let audio = AudioController.getInstance().getAudio()
        const service = new ListenAudioEventsService(audio)
        service.service()
    }
}