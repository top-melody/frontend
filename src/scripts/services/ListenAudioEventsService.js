import TrackController from 'src/scripts/controllers/TrackController'

export default class ListenAudioEventsService {

    constructor(audio) {
        this.audio = audio
    }

    service() {
        this.listenOnTimeUpdate()
        this.listenOnPause()
        this.listenOnPlay()
        this.listenOnVolumeChange()
        this.listenOnPreviousTrack()
        this.listenOnNextTrack()
        this.listenOnEnded()
    }

    listenOnTimeUpdate() {
        this.audio.ontimeupdate = () => {
            TrackController.getInstance().setCurrentPosition(this.audio.currentTime, false)
        }
    }

    listenOnPause() {
        this.audio.onpause = () => {
            TrackController.getInstance().setIsPlaying(false, false)
        }
    }

    listenOnPlay() {
        this.audio.onplay = () => {
            TrackController.getInstance().setIsPlaying(true, false)
        }
    }

    listenOnVolumeChange() {
        this.audio.onvolumechange = () => {
            const value = Math.floor(this.audio.volume * 100)
            TrackController.getInstance().setVolume(value)
        }
    }

    listenOnPreviousTrack() {
        navigator.mediaSession.setActionHandler('previoustrack', () => {
            TrackController.getInstance().decrementCurrentId()
        })
    }

    listenOnNextTrack() {
        navigator.mediaSession.setActionHandler('nexttrack', () => {
            TrackController.getInstance().incrementCurrentId()
        })
    }

    listenOnEnded() {
        this.audio.onended = () => {
            TrackController.getInstance().incrementCurrentId()
            TrackController.getInstance().setIsPlaying(true)
        }
    }
}
