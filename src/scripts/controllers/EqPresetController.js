import EqPresets from 'src/scripts/data/EqPresets'
import LocalStorageHelper from 'src/scripts/helpers/LocalStorageHelper'
import * as LocalStorageKeyEnum from 'src/scripts/enums/LocalStorageKeyEnum'
import EqFrequencyPositions from 'src/scripts/data/EqFrequencyPositions'
import * as EqLevelKeysEnum from 'src/scripts/enums/EqLevelKeysEnum'
import * as EqPresetsEnum from "src/scripts/enums/EqPresetsEnum";
import AudioController from "src/scripts/controllers/AudioController";

export default class EqPresetController {
    static instance = null

    static getInstance() {
        if (!EqPresetController.instance) {
            EqPresetController.instance = new EqPresetController()
        }

        return EqPresetController.instance
    }

    getSelectorOptions() {
        let result = []
        for (const presetNumber in EqPresets) {
            const preset = EqPresets[presetNumber]
            result.push({
                label: preset.label,
                value: preset.id
            })
        }

        return result
    }
    
    getPresetById(id) {
        for (const presetId in EqPresets) {
            const preset = EqPresets[presetId]
            if (preset.id === id) {
                return preset
            }
        }
        
        return this.getDefaultPreset()
    }

    setSelectedPresetId(id) {
        const preset = this.getPresetById(id)
        LocalStorageHelper.set(LocalStorageKeyEnum.SelectedEqPreset, preset)
        this.resetAllAudioEq()
    }

    getPresetLabel(id) {
        for (const presetNumber in EqPresets) {
            const preset = EqPresets[presetNumber]
            if (preset.id === id) {
                return preset.label
            }
        }
    }

    getSelectedPreset() {
        const preset = LocalStorageHelper.get(LocalStorageKeyEnum.SelectedEqPreset)
        return preset ?? this.getDefaultPreset()
    }

    setPreset(preset) {
        LocalStorageHelper.set(LocalStorageKeyEnum.SelectedEqPreset, preset)
    }

    getDefaultPreset() {
        return EqPresets[0]
    }
    
    setIsOn(value) {
        LocalStorageHelper.set(LocalStorageKeyEnum.IsEqOn, value)
        this.resetAllAudioEq()
    }

    getIsOn() {
        return LocalStorageHelper.get(LocalStorageKeyEnum.IsEqOn) ?? true
    }

    getFrequencyToLevelMapping() {
        const selectedPreset = this.getSelectedPreset()
        const addendumLevel = selectedPreset[EqLevelKeysEnum.CommonLevel]

        return EqFrequencyPositions.filter(p => !!p.frequency).map(position => {
            const level = this.getIsOn() ? selectedPreset[position.id] + addendumLevel : 0
            return {
                frequency: position.frequency,
                level: level // К частному уровню прибавляем общий
            }
        })
    }

    changeFrequencyValue(id, value) {
        let selectedPreset = this.getSelectedPreset()
        selectedPreset[id] = value
        selectedPreset.id = EqPresetsEnum.MY
        selectedPreset.label = EqPresetController.getInstance().getPresetLabel(EqPresetsEnum.MY)
        this.setPreset(selectedPreset)

        if (id === EqLevelKeysEnum.CommonLevel) {
            this.resetAllAudioEq()
        } else {
            const frequency = EqFrequencyPositions.filter(p => p.id === id)[0].frequency
            AudioController.getInstance().changeFrequencyLevel(frequency, value)
        }
    }

    resetAllAudioEq() {
        this.getFrequencyToLevelMapping().map(position => {
            AudioController.getInstance().changeFrequencyLevel(position.frequency, position.level)
        })
    }
}