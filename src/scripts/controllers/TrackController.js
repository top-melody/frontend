import { useTrackStore } from 'src/stores/track-store'
import LocalStorageHelper from 'src/scripts/helpers/LocalStorageHelper'
import * as LocalStorageKeyEnum from 'src/scripts/enums/LocalStorageKeyEnum'
import AudioController from 'src/scripts/controllers/AudioController'

const defaultShuffle = false
const defaultRepeat = false
const defaultVolume = 95

export default class TrackController
{
    static instance = null

    constructor() {
        this.trackStore = useTrackStore()
    }

    static getInstance() {
        if (!TrackController.instance) {
            TrackController.instance = new TrackController()
        }

        return TrackController.instance
    }

    setList(playlistId, trackList) {
        this.trackStore.list[playlistId] = trackList
    }

    getList(playlistId) {
        return this.trackStore.list[playlistId]
    }

    getIsShuffle() {
        return LocalStorageHelper.get(LocalStorageKeyEnum.IsShuffle) ?? defaultShuffle
    }

    setIsShuffle(value) {
        this.trackStore.isShuffle = value
        LocalStorageHelper.set(LocalStorageKeyEnum.IsShuffle, value)
    }

    getIsRepeat() {
        return LocalStorageHelper.get(LocalStorageKeyEnum.IsRepeat) ?? defaultRepeat
    }

    setIsRepeat(value) {
        this.trackStore.isRepeat = value
        LocalStorageHelper.set(LocalStorageKeyEnum.IsRepeat, value)
        AudioController.getInstance().setIsRepeat(value)
    }

    getVolume() {
        return LocalStorageHelper.get(LocalStorageKeyEnum.Volume) ?? defaultVolume
    }

    setVolume(value) {
        this.trackStore.volume = value
        LocalStorageHelper.set(LocalStorageKeyEnum.Volume, value)
        AudioController.getInstance().setVolume(value)
    }

    getIsPlaying() {
        return this.trackStore.isPlaying
    }

    setIsPlaying(value, changeAudioState = true) {
        this.trackStore.isPlaying = value

        if (changeAudioState) {
            value ? AudioController.getInstance().play() : AudioController.getInstance().pause()
        }
    }

    setCurrentId(id) {
        this.trackStore.currentId = id
        AudioController.getInstance().setSource(this.getStreamUrl(id))
    }

    incrementCurrentId() {
        this.setNextTrackId(this.getCurrentPosition() + 1)
    }

    decrementCurrentId() {
        this.setNextTrackId(this.getCurrentPosition() - 1)
    }

    getCurrentPosition() {
        for (const position in this.trackStore.list[this.trackStore.currentPlaylistId]) {
            const track = this.trackStore.list[this.trackStore.currentPlaylistId][position]
            if (track.id === this.trackStore.currentId) {
                return parseInt(position)
            }
        }

        return 0
    }

    setNextTrackId(newPosition) {
        let prevTrack = this.trackStore.list[this.trackStore.currentPlaylistId][newPosition];
        if (!prevTrack) {
            prevTrack = this.trackStore.list[this.trackStore.currentPlaylistId][0]
        }

        this.setCurrentId(prevTrack.id)
        this.setIsPlaying(true)
    }

    setCurrentPlaylistId(id) {
        this.trackStore.currentPlaylistId = id
    }
    
    getStreamUrl(id) {
        return 'http://localhost:8090/'
    }

    setCurrentPosition(seconds, changeAudioState = true) {
        this.trackStore.currentPosition = seconds

        if (changeAudioState) {
            AudioController.getInstance().setCurrentTime(seconds)
        }
    }
}