import TrackController from 'src/scripts/controllers/TrackController'
import EqPresetController from "src/scripts/controllers/EqPresetController";

const defaultAudioPreload = 'metadata'
const EqFiltersType = 'peaking'
const EqFiltersQ = 2

export default class AudioController {
    static instance = null
    
    constructor() {
        this.initAudio()
    }

    static getInstance() {
        if (!AudioController.instance) {
            AudioController.instance = new AudioController()
        }

        return AudioController.instance
    }
    
    initAudio() {
        // Во время применения изменения в момент разработки элемент дублируется
        this.audio = document.querySelector('audio')
        if (!this.audio) {
            this.audio = new Audio('')

            // Добавляем элемент на страницу, чтобы при запуске метода play()
            // api не ругалось на отсутствие элемента на странице
            document.querySelector('body').append(this.audio)
        }

        this.audio.preload = defaultAudioPreload
        this.setVolume(TrackController.getInstance().getVolume())
        this.setIsRepeat(TrackController.getInstance().getIsRepeat())
        this.audio.crossOrigin = 'anonymous'

        this.context = null
        this.analyzer = null
        this.filters = []
    }

    getAudio() {
        return this.audio
    }
    
    setVolume(level) {
        const value = +(level / 100).toFixed(2)
        this.audio.volume = value
    }
    
    setSource(src) {
        this.audio.src = src
        this.audio.load()
    }
    
    play() {
        return this.audio.play()
            .then(() => {
                if (!this.context) {
                    this.initContext()
                }
            }).catch(() => {})
    }

    pause() {
        this.audio.pause()
    }

    setCurrentTime(seconds) {
        this.audio.currentTime = seconds
    }

    setIsRepeat(isRepeat) {
        this.audio.loop = isRepeat
    }

    initContext() {
        const AudioContext = window.AudioContext || window.webkitAudioContext
        const context = new AudioContext()

        const source = new MediaElementAudioSourceNode(context, {mediaElement: this.audio})
        const analyzer = context.createAnalyser()
        const filters = this.initEqFilters(context)

        // Схема соединения: источник -> biquadFilter[] -> анализатор -> колонки
        source.connect(filters[0].filter)
        filters.reduce(function (prev, curr) {
            prev.filter.connect(curr.filter);
            return curr;
        })
        filters[filters.length - 1].filter.connect(analyzer)
        analyzer.connect(context.destination)

        this.context = context
        this.analyzer = analyzer
        this.filters = filters
    }

    initEqFilters(context) {
        return EqPresetController
            .getInstance()
            .getFrequencyToLevelMapping()
            .map(position => {
                const filter = context.createBiquadFilter()
                filter.type = EqFiltersType
                filter.q = EqFiltersQ
                filter.frequency.setValueAtTime(position.frequency, 0)
                filter.gain.setValueAtTime(position.level, 0)

                return {
                    frequency: position.frequency,
                    filter: filter
                }
            })
    }

    // value === 0 - нормальное значение для фильтра
    changeFrequencyLevel(frequency, level) {
        const matchFilterList = this.filters.filter(filterMapping => filterMapping.frequency === frequency)
        if (matchFilterList.length === 0) {
            return
        }

        const filter = matchFilterList[0].filter
        filter.gain.setValueAtTime(level, 0)
    }

    // https://developer.mozilla.org/ru/docs/Web/API/AnalyserNode/getByteFrequencyData
    // Возвращает массив с линейным распределением уровней громкости (от 0 до 255)
    // по диапазону от 0 до 1/2 частоты дискретизации
    getFrequenciesLevelData() {
        if (!this.analyzer || this.audio.paused) {
            return []
        }

        let array = new Uint8Array(this.analyzer.frequencyBinCount)
        this.analyzer.ttfSize = this.analyzer.frequencyBinCount
        this.analyzer.getByteFrequencyData(array)
        return array
    }
}