import LocalStorageHelper from 'src/scripts/helpers/LocalStorageHelper'
import * as LocalStorageKeyEnum from 'src/scripts/enums/LocalStorageKeyEnum'

const maxCountOfRecentQueries = 8

export default class RecentQueriesController {
    static instance = null
    
    static getInstance() {
        if (!RecentQueriesController.instance) {
            RecentQueriesController.instance = new RecentQueriesController()
        }

        return RecentQueriesController.instance
    }
    
    getList() {
        const queries = LocalStorageHelper.get(LocalStorageKeyEnum.RecentQueries)
        return queries ?? []
    }
    
    setOne(searchText) {
        let queries = this.getList()
        if (!queries.includes(searchText)) {
            if (queries.length >= maxCountOfRecentQueries) {
                queries.shift()
            }

            queries.push(searchText)
        }

        LocalStorageHelper.set(LocalStorageKeyEnum.RecentQueries, queries)
    }
    
    removeAll() {
        LocalStorageHelper.remove(LocalStorageKeyEnum.RecentQueries)
    }
}