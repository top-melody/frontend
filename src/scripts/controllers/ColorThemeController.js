import LocalStorageHelper from 'src/scripts/helpers/LocalStorageHelper'
import * as LocalStorageKeyEnum from 'src/scripts/enums/LocalStorageKeyEnum'
import { Dark, setCssVar } from 'quasar'

const lightColors = [
    {
        id: 'background-secondary',
        color: '#fafafa'
    },
    {
        id: 'body-background',
        color: '#ffffff'
    },
    {
        id: 'text-primary',
        color: '#333333'
    },
    {
        id: 'text-secondary',
        color: 'rgba(51, 51, 51, 0.7)'
    },
    {
        id: 'accent',
        color: '#b2dfdb'
    }
]

const darkColors = [
    {
        id: 'background-secondary',
        color: '#1D1D1D'
    },
    {
        id: 'body-background',
        color: '#121212'
    },
    {
        id: 'text-primary',
        color: '#cccccc'
    },
    {
        id: 'text-secondary',
        color: 'rgba(204, 204, 204, 0.7)'
    },
    {
        id: 'accent',
        color: '#121212'
    }
]

export default class ColorThemeController {
    static instance = null

    static getInstance() {
        if (!ColorThemeController.instance) {
            ColorThemeController.instance = new ColorThemeController()
        }

        return ColorThemeController.instance
    }

    setCurrentTheme() {
        const isDark = this.getIsDarkTheme()
        Dark.set(isDark)

        const colors = isDark ? darkColors : lightColors
        for(const i in colors) {
            const color = colors[i]
            // id должны писаться в стиле kebab-case,
            // они преобразуются в выражение "--q-kebab-case",
            // которые можно использовать в стилях как "var(--q-kebab-case)"
            setCssVar(color.id, color.color)
        }
    }

    getIsDarkTheme() {
        return LocalStorageHelper.get(LocalStorageKeyEnum.IsDarkTheme) ?? false
    }

    setIsDarkTheme(value) {
        LocalStorageHelper.set(LocalStorageKeyEnum.IsDarkTheme, value)
        this.setCurrentTheme()
    }
}