import * as PageRouteEnum from 'src/scripts/enums/PageRouteEnum'
import Anchors from 'src/scripts/data/Anchors'

const getTitleByLink = (link) => {
  const anchor = Anchors.filter(a => a.link === link)
  return anchor[0].title
}

const routes = [
  {
    path: '/',
    redirect: PageRouteEnum.Track
  },
  {
    path: PageRouteEnum.Track,
    component: () => import('layouts/main-layout.vue'),
    children: [
      { path: '', component: () => import('pages/track.vue') }
    ],
    meta: {
      title: getTitleByLink(PageRouteEnum.Track)
    }
  },
  {
    path: PageRouteEnum.Playlist,
    component: () => import('layouts/main-layout.vue'),
    children: [
      { path: '', component: () => import('pages/playlist.vue') }
    ],
    meta: {
      title: getTitleByLink(PageRouteEnum.Playlist)
    }
  },
  {
    path: PageRouteEnum.Eq,
    component: () => import('layouts/main-layout.vue'),
    children: [
      { path: '', component: () => import('pages/eq.vue') }
    ],
    meta: {
      title: getTitleByLink(PageRouteEnum.Eq)
    }
  },
  {
    path: PageRouteEnum.Account,
    component: () => import('layouts/main-layout.vue'),
    children: [
      { path: '', component: () => import('pages/account.vue') }
    ],
    meta: {
      title: getTitleByLink(PageRouteEnum.Account)
    }
  },
  {
    path: PageRouteEnum.Auth,
    component: () => import('layouts/simple-layout.vue'),
    children: [
      { path: '', component: () => import('pages/auth.vue') }
    ],
    meta: {
      title: 'Вход'
    }
  },
  {
    path: PageRouteEnum.Register,
    component: () => import('layouts/simple-layout.vue'),
    children: [
      { path: '', component: () => import('pages/register.vue') }
    ],
    meta: {
      title: 'Регистрация'
    }
  },
  {
    path: PageRouteEnum.PasswordRecovery,
    component: () => import('layouts/simple-layout.vue'),
    children: [
      { path: '', component: () => import('pages/password-recovery.vue') }
    ],
    meta: {
      title: 'Восстановление'
    }
  },
  {
    path: PageRouteEnum.NewPassword,
    component: () => import('layouts/simple-layout.vue'),
    children: [
      { path: '', component: () => import('pages/new-password.vue') }
    ],
    meta: {
      title: 'Новый пароль'
    }
  },
  {
    path: PageRouteEnum.About,
    component: () => import('layouts/main-layout.vue'),
    children: [
      { path: '', component: () => import('pages/about.vue') }
    ],
    meta: {
      title: getTitleByLink(PageRouteEnum.About)
    }
  },
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/error-not-found.vue')
  }
]

export default routes
